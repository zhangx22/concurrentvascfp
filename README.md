# ConcurrentVaScFP

This project host the compiled code and input files for generating the results in the paper: Rapid Synchronized Fabrication of Vascularized Thermosetsand Composites

Contact: If you have any questions, please contact xiang zhang by email: xiang.zhang@uwyo.edu

Environemnt
1) This code is developed as an user applicaiton based on the Multiphysics Object-Oriented Simulation Environment (MOOSE). MOOSE is open access to the pbulic at https://github.com/idaholab/moose, and the version of MOOSE used for our development has an commit number of: 002a1b69750b93bd97505b6f44fc554581ac6bd4. 
2) This version of MOOSE is build in a Red Hat Enterprise Linux Workstation 7.9 OS, with the dependent libriaries: GCC 7.2.0, MPICH 3.2.1 and PETSc 3.8.3. The compiled version of the Application is comiled in the same system. Moose instalaltion procedure is listed at: https://mooseframework.inl.gov/getting_started/installation/manual_installation_llvm.html
3) Depending on your system, the the installation of MOOSE and its dependencies may take a few hours. 

Instructions for running a concurrent vascularization and frontal polymerization:
1) Make sure to use a similar linux system, with the same version of dependent libraries
2) Export the path to the system path of those dependent libraries, which can be done using the included .sh file: source ./moose_environment.sh
2) cd to the folder where the compiled exectuable (army_ants-opt) and input file (W30-Glass07mmAirh25-A025.i)  and mesh file (W30-Glass07mm.inp) are located 
3) To run the simulation in serial: ./army_ants-opt -i W30-Glass07mmAirh25-A025.i
4) To run the simualtion in paralle: mpiexec -n 4  ./army_ants-opt -i W30-Glass07mmAirh25-A025.i, whre the user can change the number of cpus from 4 to other numbers depend on the system. 
5) As a reference, the computer the authors used to run the simulation has Intel(R) Xeon(R) Gold 6240 CPU @ 2.60GHz processors, and it took about 10 minuts to run the above simualtion to time=35s when using 18 cpus; and for the case alpha_0=0.35, it takes 30 mintues using 18 cpus
6) Once the simualtions are done, the result files (*.e) are loaded into ParaView for visualization and postprocessing. The ParaView version used is ParaView-5.6.2-MPI-Linux-64bit.  
6) If one wants to run a different simualtion (e.g., change initial conditions, boundary conditions, simulation domains, etc.), change the input file and the mesh file accourdingly. The MOOSE input file syntax manual (https://mooseframework.inl.gov/bison/syntax/index.html), as well as the comments in the input files serves as a good starting point for this purpose. 
