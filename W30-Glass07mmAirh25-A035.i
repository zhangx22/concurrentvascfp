#-- This is an exmaple input file, which was used to generated the resutls for SI Figure 2 (d) 
#-- in the paper: RapidSynchronized Fabrication of Vascularized Thermosetsand Composites
#-- If you have any questions, please contact xiang zhang by email: xiang.zhang@uwyo.edu
#
#
[Mesh]
  file = W30-Glass07mm.inp
  uniform_refine = 0
  construct_side_list_from_node_list = true
[]

[Variables]
  active = 'Temperature Cure Degredation'

  [./Temperature]
    order = FIRST
    family = LAGRANGE
    initial_condition = '23' #-- room temperature, change to other initial temperature as needed
    block = '0 1 2'
  [../]
  
  [./Cure]
    order = FIRST
    family = LAGRANGE
    initial_condition = '0.35' #-- Initial degree of cure, change as needed
    block = '0'
  [../]

  [./Degredation]
    order = FIRST
    family = LAGRANGE
    initial_condition = '0.01' #-- Initial degree of degradation, note zero will cause numerical issue,  
    block = '2'                #-- so we use a small number close to zero to represent zero initial degree of cure
  [../]

[]

[Kernels]

  [./tempdiff] #temperature diffusion term
    type = TempDiffusion
    variable = Temperature
    block = '0 1 2'
  [../]

  [./coupledcurederv]
    type = CoupledCureTimeDerivative
    variable = Temperature
    v = Cure
    block = '0'    
  [../]

  [./coupleddegredationderv]
    type = CoupledCureTimeDerivative
    variable = Temperature
    v = Degredation
    block = '2'    
  [../]
  
 [./tempderv]
  type = HeatConductionTimeDerivative
  variable = Temperature
  lumping=false
  block = '0 1 2'
  [../]
  
  [./curederv]
  type=TimeDerivative
  variable = Cure
  lumping=false
    block = '0'
  [../]  

  [./degredationderv]
  type=TimeDerivative
  variable = Degredation
  lumping=false
    block = '2'
  [../]  
  
 [./cureformula]
  type=DCPDnonDgeneralPT
  variable = Cure
  v = Temperature #this is the coupled variable
  block = '0'
  Ttrig = '1'
  Tintl = '0'
  _E= '113221'                                #-- cure kinetics parameters in SI Table 4
  _n= '1.74'
  _m= '0.78'
  _cd= '15.2'
  _ad= '0.71'                                 #-- note we combined the alpha_0 and alpha_C together into one parameter
  [../]

 [./degradationformula]
  type=CPPAnonDgeneralPT
  variable = Degredation
  v = Temperature #this is the coupled variable
  block = '2'
  Ttrig = '1'
  Tintl = '0'                                 #-- depolymerization kinetics parameters in SI Table 4
  _E= '110013'
  _n= '0.9415'
  _m= '0.208'
  [../]

[]

[BCs]

  [./Initiation]
    type = DirichletBC
   variable = Temperature
    boundary = Initiation
    value = '160'                       #-- We are applying heating at 160C for 4.5s as indicated in the control block
  [../]
#--Apply a convective BC at the top surface
   [./Convection]    
     type = ConvectiveFluxFunction      # Convective flux, e.g. q'' = h*(T - #T_infinity)
     boundary = TOPFACE
     variable = Temperature
     coefficient = 25                   # convective heat transfer #coefficient (w/m^2-K)
     T_infinity = 23
   [../]                                 
[]

#length is 5*30mm, Rm is 5mm
[Materials]
  [./DCPD]
   block = 0
    type = GenericConstantMaterial

    prop_names = 'specific_heat Hr density TConductivity A'
    prop_values = '1600 380000 980 0.152 8.55e15'  # polymer attributes
  [../]

  [./PPC]
   block = 2
    type = GenericConstantMaterial

    prop_names = 'specific_heat Hr density TConductivity A'
    prop_values = '1800 -20000 1313 0.25  4.0e12'  # polymer attributes, densiticity is 1313kg/m^3 according to Mayank
  [../]

  [./GLASS]
    type = GenericConstantMaterial
    block = '1'
    prop_names = 'specific_heat density  TConductivity '         
    prop_values = '800 2000  1.14 ' #highkappa attributes
  [../]

[]

[Problem]  #makes the problem axisymmetric
  type = FEProblem
  coord_type = RZ
  rz_coord_axis = X
[]

[Executioner]
  type = Transient
  num_steps = 80000
  # num_steps = 1000
  nl_rel_tol = 1e-9

  end_time=35

  nl_max_its=10
  l_max_its=15
  
  [./TimeStepper]
    type = ConstantDT    
    dt = 0.1
    # dt = 3.6e14 
  [../]
  
  [./TimeIntegrator]
   type = ImplicitEuler
[../]
  
  #Preconditioned JFNK (default)
  solve_type = 'PJFNK'
  petsc_options_iname = '-pc_type -pc_hypre_type'
  petsc_options_value = 'hypre boomeramg'
  #petsc_options_iname = '-pc_type'
  #petsc_options_value = 'gamg'
  
[]

[Adaptivity]
  marker = combo #this line when commented, switches off adaptivity
  max_h_level = 6
  #steps = 2 #this line gets ignored in a transient run
  [./Indicators]
    [./Aerror]
      type = GradientJumpIndicator
      variable = Cure
      outputs = none
    [../]

    [./Berror]
      type = GradientJumpIndicator
      variable = Degredation
      outputs = none
    [../]

    [./Terror]
      type = GradientJumpIndicator
      variable = Temperature
      outputs = none
    [../]
    [./error]
      type = GradientJumpIndicator
      variable = Temperature
      outputs = none
    [../]
    
  [../]
  [./Markers]
    [./Marker1]
      type = ErrorFractionMarker
      refine = 0.6
      coarsen = 0.25
      indicator = Aerror
      outputs = none
    [../]

    [./Marker2]
      type = ErrorFractionMarker
      refine = 0.6
      coarsen = 0.25
      indicator = Berror
      outputs = none
    [../]

    [./Marker3]
      type = ErrorFractionMarker
      refine = 0.6
      coarsen = 0.25
      indicator = Terror
      outputs = none
    [../]

    [./combo]
      type = ComboMarker
      markers = 'Marker1 Marker2 Marker3'
    [../]
    
  [../]
  [./Markers]
    [./errorfrac]
      type = ErrorFractionMarker
      refine = 0.6
      coarsen = 0.25
      indicator = error
      outputs = none
    [../]
    
  [../]
[]


[Controls]
  [./bcs]
    type = TimePeriod
    disable_objects = 'BCs::Initiation'
    start_time = '4.5' #'1718880'  '1317808'   
    execute_on = 'initial timestep_begin'
  [../]   
[]

[Outputs] 
  execute_on = 'timestep_end' # Limit the output to timestep end (removes initial condition)
  [./console]
    type = Console
    perf_log = true          # enable performance logging    
  [../]
  [./exodus]
    type = Exodus
    execute_on = 'initial timestep_end' # output the initial condition for the file
    file_base = ./OutputW30-Glass07mmAirh25-A035/A035-Glass07mm # set the file base (the extension is automatically applied) 
    interval = 1           # only output every 1 step
  [../]  
[]

