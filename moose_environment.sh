#!/bin/bash
### MOOSE Environment Profile
# GCC 7.2.0
# MPICH 3.2.1
# PETSc 3.8.3

export PACKAGES_DIR=/home/xzhang16/Projects/


export  PATH=$PACKAGES_DIR/gcc-7.2.0/bin:$PACKAGES_DIR/mpich-3.2.1/bin:$PACKAGES_DIR/miniconda2/bin:$PATH
export LD_LIBRARY_PATH=$PACKAGES_DIR/gcc-7.2.0/lib64:$PACKAGES_DIR/gcc-7.2.0/lib:$PACKAGES_DIR/gcc-7.2.0/lib/gcc/x86_64-pc-linux-gnu/7.2.0:$PACKAGES_DIR/gcc-7.2.0/libexec/gcc/x86_64-pc-linux-gnu/7.2.0:$PACKAGES_DIR/mpich-3.2.1/lib:$LD_LIBRARY_PATH
export C_INCLUDE_PATH=$PACKAGES_DIR/mpich-3.2.1/include:$C_INCLUDE_PATH
export CPLUS_INCLUDE_PATH=$PACKAGES_DIR/mpich-3.2.1/include:$CPLUS_INCLUDE_PATH
export FPATH=$PACKAGES_DIR/mpich-3.2.1/include:$FPATH
export MANPATH=$PACKAGES_DIR/mpich-3.2.1/share/man:$MANPATH
export PETSC_DIR=$PACKAGES_DIR/petsc-3.8.3
export CC=mpicc
export CXX=mpicxx
export FC=mpif90
export F90=mpif90
